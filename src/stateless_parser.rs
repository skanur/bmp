use nom::*;

use std;
use num;

use errors::BMPError;

// Some helper functions that return BMPError
named!(pu16<&[u8], u16, BMPError>, fix_error!(BMPError, u16!(false)));
named!(pu32<&[u8], u32, BMPError>, fix_error!(BMPError, u32!(false)));
named!(pi32<&[u8], i32, BMPError>, fix_error!(BMPError, i32!(false)));

#[derive(Debug, PartialEq)]
pub enum BMPType {
    BM,
}

// Parse for BMPHeader
named!(pub parse_bm<&[u8], BMPType, BMPError> , fix_error!(BMPError, value!(BMPType::BM, tag!(b"BM"))));

#[derive(Debug, PartialEq)]
pub struct BMPHeader {
    pub bmp_type: BMPType,
    pub bmp_size: u32,
    pub bmp_offset: u32,
}

named!(pub parse_bmp_header<&[u8], BMPHeader, BMPError>, chain!(
        b: parse_bm ~
        s : pu32 ~
        fix_error!(BMPError, take!(2)) ~
        fix_error!(BMPError, take!(2)) ~
        o: pu32,
        || { BMPHeader{bmp_type: b, bmp_size : s, bmp_offset : o} }
        )
    );

// Core header specifics
#[derive(Debug, PartialEq)]
pub struct CoreHeader {
    pub bc_width: u16,
    pub bc_height: u16,
    pub bc_planes: u16,
    pub bc_bit_count: u16,
}

named!(pub valid_planes<&[u8], u16, BMPError>, add_error!(ErrorKind::Custom(BMPError::FormatError), fix_error!(BMPError, switch!(
                u16!(false),
                1 => apply!(id, 1)
                ))));

// Useful function to return dummy result
fn id<'a, N>(input: &'a [u8], value: N) -> IResult<&'a [u8], N> {
    IResult::Done(input, value)
}

named!(pub valid_bit_count<&[u8], u16, BMPError>, add_error!(ErrorKind::Custom(BMPError::FormatError), fix_error!(BMPError, switch!(
                u16!(false),
                0 => apply!(id, 0) |
                1 => apply!(id, 1) |
                4 => apply!(id, 4) |
                8 => apply!(id, 8) |
                16 => apply!(id, 16) |
                24 => apply!(id, 24) |
                32 => apply!(id, 32)
                ))));

named!(pub parse_core<&[u8], CoreHeader, BMPError>, chain!(
        wd: pu16 ~
        ht: pu16 ~
        pl: valid_planes ~
        bc: valid_bit_count,
        || { CoreHeader {
            bc_width : wd,
            bc_height : ht,
            bc_planes : pl,
            bc_bit_count: bc,
        }}
        )
    );

// Info header specifics
#[derive(Debug, PartialEq)]
pub struct BitmapInfo<'a> {
    pub bmi_header: InfoHeader,
    pub bmi_colors: Vec<Quads>,
    pub pixels: &'a [u8],
}

#[derive(Debug, PartialEq)]
pub enum Quads {
    DwordQuad { red: u32, green: u32, blue: u32 },
    RGBQuad {
        blue: u8,
        green: u8,
        red: u8,
        alpha: u8,
    },
}

named!(dwordquad<&[u8], Quads, BMPError>, fix_error!(BMPError, chain!(
            r: le_u32 ~
            g: le_u32 ~
            b: le_u32,
            || {
                Quads::DwordQuad{
                    red: r,
                    green: g,
                    blue: b,
                }
            }
            )));

named!(rgbquad<&[u8], Quads, BMPError>, fix_error!(BMPError, chain!(
        b: le_u8 ~
        g: le_u8 ~
        r: le_u8 ~
        a: le_u8,
        || {
            Quads::RGBQuad {
                blue : b,
                green: g,
                red  : r,
                alpha: a,
            }
        }
        )));

#[derive(Debug, PartialEq)]
pub enum Compression {
    // Uncompressed format
    RGB,

    // A run-length encoded (RLE) format for bitmaps with 8 bpp. The compression format is a 2-byte
    // format consisting of a count byte followed by a byte containing a color index. For more
    // information, see Bitmap Compression.
    RLE8,

    // An RLE format for bitmaps with 4 bpp. The compression format is a 2-byte format consisting
    // of a count byte followed by two word-length color indexes. For more information,
    // see Bitmap Compression.
    RLE4,

    // Specifies that the bitmap is not compressed and that the color table consists of three DWORD
    // color masks that specify the red, green, and blue components, respectively, of each pixel.
    // This is valid when used with 16- and 32-bpp bitmaps.
    BITFIELDS,

    // Indicates a JPEG image
    JPEG,

    // Indicates a PNG image
    PNG,
}

named!(parse_compression<&[u8], Compression>, alt!(
            tag!([0x00, 0x00, 0x00, 0x00]) => { |_| Compression::RGB } |
            tag!([0x01, 0x00, 0x00, 0x00]) => { |_| Compression::RLE8 } |
            tag!([0x02, 0x00, 0x00, 0x00]) => { |_| Compression::RLE4 } |
            tag!([0x03, 0x00, 0x00, 0x00]) => { |_| Compression::BITFIELDS } |
            tag!([0x04, 0x00, 0x00, 0x00]) => { |_| Compression::JPEG } |
            tag!([0x05, 0x00, 0x00, 0x00]) => { |_| Compression::PNG }
            ));

#[derive(Debug, PartialEq)]
pub struct InfoHeader {
    pub bi_width: i32,
    pub bi_height: i32,
    pub bi_planes: u16,
    pub bi_bit_count: u16,
    pub bi_compr: Compression,
    pub bi_image_size: u32,
    pub bi_x_pixels: i32,
    pub bi_y_pixels: i32,
    pub bi_clr_used: u32,
    pub bi_clr_imp: u32,
}

named!(pub parse_info<&[u8], InfoHeader, BMPError>, chain!(
        wd: pi32 ~
        ht: pi32 ~
        pl: valid_planes ~
        bc: valid_bit_count ~
        cr: fix_error!(BMPError, parse_compression) ~
        iz: pu32 ~
        xp: pi32 ~
        yp: pi32 ~
        cu: pu32 ~
        ci: pu32,
        || {
            InfoHeader {
                bi_width: wd,
                bi_height: ht,
                bi_planes: pl,
                bi_bit_count: bc,
                bi_compr: cr,
                bi_image_size: iz,
                bi_x_pixels: xp,
                bi_y_pixels: yp,
                bi_clr_used: cu,
                bi_clr_imp: ci,
            }
        }
));

fn num_clrs<N: num::Num + std::cmp::Ord + std::marker::Copy>(clrs: N, max: N) -> N {
    if clrs.is_zero() {
        max
    } else {
        std::cmp::min(clrs, max)
    }
}

fn parse_16_32_bit_case<'a>(input: &'a [u8],
                            cmpr: &Compression,
                            clr: &u32)
                            -> IResult<&'a [u8], Vec<Quads>, BMPError> {
    match *cmpr {
        Compression::RGB => {
            if *clr > 0 {
                count!(input, rgbquad, *clr as usize)
            } else {
                count!(input, rgbquad, 0 as usize)
            }
        }
        Compression::BITFIELDS => {
            if *clr > 0 {
                chain!(input, 
                dw: count!(dwordquad, 1 as usize) ~
                count!(rgbquad, *clr as usize),
                || {
                    dw
                })
            } else {
                count!(input, dwordquad, 1 as usize)
            }
        }
        _ => IResult::Error(Err::Code(ErrorKind::Custom(BMPError::NotSupported))),
    }
}

fn rowsize(info: &InfoHeader) -> usize {
    let bits_per_pixel = info.bi_bit_count as f64;
    let image_width = info.bi_width as f64;
    (f64::floor((bits_per_pixel * image_width + 31f64) / 32f64) * 4f64) as usize
}

fn colsize(info: &InfoHeader) -> usize {
    if info.bi_height > 0 {
        info.bi_height as usize
    } else {
        let col = (info.bi_image_size) / (rowsize(&info) as u32);
        col as usize
    }
}

named!(pub parse_info_bitmap<&[u8], BitmapInfo, BMPError>, chain!(
        info  : parse_info ~
        clr_tb: switch!(
            fix_error!(BMPError, apply!(id, info.bi_bit_count)),
// 0 This is not yet supported

// When bit count is 1 -  The bitmap is monochrome, and the bmiColors member of BITMAPINFO
// contains two entries. Each bit in the bitmap array represents a pixel. If the bit is clear,
// the pixel is displayed with the color of the first entry in the bmiColors table; if the bit
// is set, the pixel has the color of the second entry in the table.
            1 => count!(rgbquad, 2) |

// When bit count is 4 - The bitmap has a maximum of 16 colors, and the bmiColors member of
// BITMAPINFO contains up to 16 entries. Each pixel in the bitmap is represented by a 4-bit
// index into the color table. For example, if the first byte in the bitmap is 0x1F, the byte
// represents two pixels. The first pixel contains the color in the second table entry, and the
// second pixel contains the color in the sixteenth table entry.
            4 => count!(rgbquad, num_clrs::<u32>(info.bi_clr_used, 16u32) as usize) |

// When bit count is 8 - The bitmap has a maximum of 256 colors, and the bmiColors member of
// BITMAPINFO contains up to 256 entries. In this case, each byte in the array represents a single pixel.
            8 => count!(rgbquad, num_clrs::<u32>(info.bi_clr_used, 256u32) as usize) |

// The bitmap has a maximum of 2^16 colors. If the biCompression member of the BITMAPINFOHEADER is BI_RGB,
// the bmiColors member of BITMAPINFO is NULL. Each WORD in the bitmap array represents a single pixel.
// The relative intensities of red, green, and blue are represented with five bits for each color component.
// The value for blue is in the least significant five bits, followed by five bits each for green and red.
// The most significant bit is not used. The bmiColors color table is used for optimizing colors used on
// palette-based devices, and must contain the number of entries specified by the biClrUsed member of the
// BITMAPINFOHEADER. If the biCompression member of the BITMAPINFOHEADER is BI_BITFIELDS, the bmiColors member
// contains three DWORD color masks that specify the red, green, and blue components, respectively, of each
// pixel. Each WORD in the bitmap array represents a single pixel. When the biCompression member is BI_BITFIELDS,
// bits set in each DWORD mask must be contiguous and should not overlap the bits of another mask.
// All the bits in the pixel do not have to be used.
            16 => call!(parse_16_32_bit_case, &info.bi_compr, &info.bi_clr_used) |

// The bitmap has a maximum of 2^24 colors, and the bmiColors member of BITMAPINFO is NULL. Each 3-byte triplet
// in the bitmap array represents the relative intensities of blue, green, and red, respectively, for a pixel.
// The bmiColors color table is used for optimizing colors used on palette-based devices, and must contain the
// number of entries specified by the biClrUsed member of the BITMAPINFOHEADER
            24 => call!(parse_16_32_bit_case, &info.bi_compr, &info.bi_clr_used) |

// The bitmap has a maximum of 2^32 colors. If the biCompression member of the BITMAPINFOHEADER is BI_RGB,
// the bmiColors member of BITMAPINFO is NULL. Each DWORD in the bitmap array represents the relative intensities of blue,
// green, and red for a pixel. The value for blue is in the least significant 8 bits, followed by 8 bits each for
// green and red. The high byte in each DWORD is not used. The bmiColors color table is used for optimizing
// colors used on palette-based devices, and must contain the number of entries specified by the biClrUsed member
// of the BITMAPINFOHEADER. If the biCompression member of the BITMAPINFOHEADER is BI_BITFIELDS, the bmiColors member contains
// three DWORD color masks that specify the red, green, and blue components, respectively, of each pixel. Each DWORD in the
// bitmap array represents a single pixel. When the biCompression member is BI_BITFIELDS, bits set in each DWORD mask must be
// contiguous and should not overlap the bits of another mask. All the bits in the pixel do not need to be used.
            32 => call!(parse_16_32_bit_case, &info.bi_compr, &info.bi_clr_used)
            ) ~
            p : fix_error!(BMPError, take!(rowsize(&info) * colsize(&info))),
            || {
                BitmapInfo {
                    bmi_header: info,
                    bmi_colors: clr_tb,
                    pixels    : p,
                }
            }
            ));

named!(pub parse_bitmap<&[u8], BitmapInfo, BMPError>, chain!(
        parse_bmp_header ~
        b_info: switch!(pu32,
        40 => call!(parse_info_bitmap)
        ),
        || {
            b_info
        }
        ));

pub struct V4Header;

pub struct V5Header;

#[derive(Clone, Debug, PartialEq)]
pub enum DIBHeader {
    CoreHeader,
    Infoheader,
    V4Header,
    V5Header,
}

pub struct BMP<'a> {
    pub header_type: BMPHeader,
    pub dib_type: DIBHeader,
    pub data: &'a [u8],
}
