use std::fmt;

#[derive(Debug, PartialEq)]
pub enum BMPError {
    FormatError,
    NotSupported,
    Unknown,
}

impl fmt::Display for BMPError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            BMPError::FormatError => write!(f, "BMP file is corrupted."),
            BMPError::NotSupported => write!(f, "Feature not supported."),
            BMPError::Unknown => write!(f, "Unknown error."),
        }
    }
}
