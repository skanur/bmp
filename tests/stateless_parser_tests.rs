extern crate bmp;

#[macro_use]
extern crate nom;

use nom::IResult::*;

use std::fs::*;
use std::io::Read;

use bmp::stateless_parser::*;
use bmp::errors::*;


macro_rules! get_input {
    ( $fname:expr) => {
        {
            let mut buf : Vec<u8> = Vec::new();
            let mut file = File::open($fname).expect("File could not be opened!");
            file.read_to_end(&mut buf).expect("Could not read file");
            buf
        }
    };
}

#[test]
fn rgb32bf_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/rgb32bf.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0020);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::BITFIELDS);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00007f00);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000000);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 1);
}

#[test]
fn rgb32_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/rgb32.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0020);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00007f00);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000000);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 0);
}

#[test]
fn rgb24pal_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/rgb24pal.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0018);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00006000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000100);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    // Even though its legal to have a pallete, I'm not parsing it.
    // assert_eq!(bi_info.bmi_colors.len(), 0);
}

#[test]
fn rgb24_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/rgb24.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0018);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00006000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000000);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 0);
}

#[test]
fn rgb16_565pal_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/rgb16-565pal.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0010);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::BITFIELDS);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00004000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000100);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    // Even though palette is legal in 16 bit, I don't parse it
    // assert_eq!(bi_info.bmi_colors.len(), 256);
}

#[test]
fn rgb16_565_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/rgb16-565.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0010);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::BITFIELDS);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00004000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000000);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 1);
}

#[test]
fn rgb16_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/rgb16.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0010);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00004000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000000);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 0);
}

#[test]
fn pal8nonsquare_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal8nonsquare.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000020);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0008);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00001000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000589);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x000000fc);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 252);
}

#[test]
fn pal8topdown_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal8topdown.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, -64);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0008);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00002000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x000000fc);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 252);
}

#[test]
fn pal8w124_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal8w124.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007c);
    assert_eq!(bi_info.bmi_header.bi_height, 0x0000003d);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0008);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00001d8c);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x000000fc);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 252);
}

#[test]
fn pal8w125_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal8w125.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007d);
    assert_eq!(bi_info.bmi_header.bi_height, 0x0000003e);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0008);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00001f00);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x000000fc);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 252);
}

#[test]
fn pal8w126_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal8w126.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007e);
    assert_eq!(bi_info.bmi_header.bi_height, 0x0000003f);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0008);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00001f80);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x000000fc);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 252);
}

#[test]
fn pal8gs_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal8gs.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0008);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00002000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x000000fc);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 252);
}

#[test]
fn pal8_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal8.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0008);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00002000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x000000fc);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 252);
}

#[test]
fn pal8_0_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal8-0.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0008);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00000000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x000000);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x000000);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000000);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 256);
}

#[test]
fn pal4gs_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal4gs.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0004);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00001000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x0000000c);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 12);
}

#[test]
fn pal4_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal4.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0004);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00001000);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x00000b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x0000000c);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 12);
}

#[test]
fn pal1wb_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal1wb.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00000400);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x0b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x0b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000002);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 2);
}

#[test]
fn pal1_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal1.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00000400);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x0b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x0b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000002);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 2);
}

#[test]
fn pa1bg_test() {
    let in_vec: Vec<u8> = get_input!("assets/g/pal1bg.bmp");
    let input: &[u8] = in_vec.as_slice();
    let (rest, bi_info) = parse_bitmap(input).unwrap();
    assert_eq!(rest.len(), 0);
    assert_eq!(bi_info.bmi_header.bi_width, 0x0000007f);
    assert_eq!(bi_info.bmi_header.bi_height, 0x00000040);
    assert_eq!(bi_info.bmi_header.bi_planes, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_bit_count, 0x0001);
    assert_eq!(bi_info.bmi_header.bi_compr, Compression::RGB);
    assert_eq!(bi_info.bmi_header.bi_image_size, 0x00000400);
    assert_eq!(bi_info.bmi_header.bi_x_pixels, 0x0b13);
    assert_eq!(bi_info.bmi_header.bi_y_pixels, 0x0b13);
    assert_eq!(bi_info.bmi_header.bi_clr_used, 0x00000002);
    assert_eq!(bi_info.bmi_header.bi_clr_imp, 0x00000000);
    assert_eq!(bi_info.bmi_colors.len(), 2);
}

#[test]
fn parse_core_test() {
    assert_eq!(Done(&[0x21, 0x22][..],
                    CoreHeader {
                        bc_width: 0xBBAA,
                        bc_height: 0xDDCC,
                        bc_planes: 0x0001,
                        bc_bit_count: 0x0001,
                    }),
               parse_core(&[0xAA, 0xBB, 0xCC, 0xDD, 0x01, 0x00, 0x01, 0x00, 0x21, 0x22]));
}

#[test]
fn parse_core_test_fail() {
    assert_eq!(Error(nom::Err::NodePosition(nom::ErrorKind::Custom(BMPError::FormatError),
                                            &[0x01, 0x01, 0x01, 0x00, 0x21, 0x022][..],
                                            Box::new(nom::Err::Position(nom::ErrorKind::Fix,
                                                                        &[0x01, 0x01, 0x01,
                                                                         0x00, 0x21, 0x22][..])))),
               parse_core(&[0xAA, 0xBB, 0xCC, 0xDD, 0x01, 0x01, 0x01, 0x00, 0x21, 0x22]));
    assert_eq!(Error(nom::Err::NodePosition(nom::ErrorKind::Custom(BMPError::FormatError),
                                            &[0x01, 0x01, 0x21, 0x022][..],
                                            Box::new(nom::Err::Position(nom::ErrorKind::Fix,
                                                                        &[0x01,
                                                                         0x01, 0x21, 0x22][..])))),
               parse_core(&[0xAA, 0xBB, 0xCC, 0xDD, 0x01, 0x00, 0x01, 0x01, 0x21, 0x22]));
}

#[test]
fn parse_bmp_header_test() {
    assert_eq!(Done(&[0x31, 0x32][..],
                    BMPHeader {
                        bmp_type: BMPType::BM,
                        bmp_size: 0xDDCCBBAA,
                        bmp_offset: 0x05040302,
                    }),
               parse_bmp_header(&[0x42, 0x4D, 0xAA, 0xBB, 0xCC, 0xDD, 0x00, 0x01, 0x02, 0x03,
                                  0x02, 0x03, 0x04, 0x05, 0x31, 0x32]));
}

#[test]
fn parse_bm_test() {
    assert_eq!(Done(&b"12"[..], BMPType::BM), parse_bm(&b"BM12"[..]));
}

#[test]
fn parse_bm_fail_test() {
    // Garbage inbetween
    assert_eq!(Error(nom::Err::Position(nom::ErrorKind::Fix, &b"1BM"[..])),
               parse_bm(&b"1BM"[..]));
    assert_eq!(Error(nom::Err::Position(nom::ErrorKind::Fix, &b"B1M"[..])),
               parse_bm(&b"B1M"[..]));

    // Different cases
    assert_eq!(Error(nom::Err::Position(nom::ErrorKind::Fix, &b"bm1"[..])),
               parse_bm(&b"bm1"[..]));
    assert_eq!(Error(nom::Err::Position(nom::ErrorKind::Fix, &b"Bm1"[..])),
               parse_bm(&b"Bm1"[..]));
    assert_eq!(Error(nom::Err::Position(nom::ErrorKind::Fix, &b"bM1"[..])),
               parse_bm(&b"bM1"[..]));

    // Unexpected EOF as well as wrong beginning
    assert_eq!(Error(nom::Err::Position(nom::ErrorKind::Fix, &b"M"[..])),
               parse_bm(&b"M"[..]));
}

#[test]
fn parse_bm_need_test() {
    // Unexpected EOF
    assert_eq!(Incomplete(nom::Needed::Size(2)), parse_bm(&b"B"[..]));
}
